![Tripetto](https://unpkg.com/tripetto/assets/banner.svg)

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Mailer block
[![Status](https://gitlab.com/tripetto/blocks/mailer/badges/master/pipeline.svg)](https://gitlab.com/tripetto/blocks/mailer/commits/master)
[![Styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![Docs](https://img.shields.io/badge/docs-website-blue.svg)](https://docs.tripetto.com/guide/blocks)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

This block for Tripetto is an action block. It sends a fully customizable email after the form is submitted.

[![Try the demo](https://unpkg.com/tripetto/assets/button-demo.svg)](https://codepen.io/tripetto/debug/jgNVeo)
[![View the code](https://unpkg.com/tripetto/assets/button-codepen.svg)](https://codepen.io/tripetto/pen/jgNVeo)

# Get started
You need to install or import this block to use it in Tripetto. If you are using the CLI version of the builder, you can find instructions [here](https://docs.tripetto.com/guide/builder/#cli-configuration). If you are embedding the builder into your own project using the library, take a look [here](https://docs.tripetto.com/guide/builder/#library-blocks).

# Use cases
- Directly in your browser (add `<script src="https://unpkg.com/tripetto-block-mailer"></script>` to your HTML);
- In the CLI builder (install the block using `npm i tripetto-block-mailer -g` and update your Tripetto [config](https://docs.tripetto.com/guide/builder/#cli-configuration));
- In your builder implementation (just add `import "tripetto-block-mailer";` to your code);
- In your runner implementation (simply add `import "tripetto-block-mailer/runner";` to your code. By default the `ES5` version is used. Append `/es5` or `/es6` to your import to request a specific version (for example `import "tripetto-block-mailer/runner/es6";` for the `ES6` version).

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/blocks/mailer/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# License
Have a blast. [MIT](https://opensource.org/licenses/MIT).

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
