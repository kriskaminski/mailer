/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "tripetto";
import { Mailer } from "./";
import { IMailerCondition } from "../runner/interface";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DELIVERY from "../../assets/icon-delivery.svg";
import ICON_NO_DELIVERY from "../../assets/icon-no-delivery.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Mailer,
    icon: ICON,
    get label() {
        return pgettext("block:mailer", "Mail status");
    },
})
export class MailerCondition
    extends ConditionBlock
    implements IMailerCondition {
    @definition
    @affects("#name")
    readonly willMail: boolean = true;

    get icon() {
        return this.willMail ? ICON_DELIVERY : ICON_NO_DELIVERY;
    }

    get name() {
        return this.willMail
            ? pgettext("block:mailer", "Mail will be send")
            : pgettext("block:mailer", "No mail will be send");
    }
}
