/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Components,
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    filter,
    isBoolean,
    isString,
    map,
    pgettext,
    populateSlots,
    slots,
    tripetto,
} from "tripetto";
import { IMailer } from "../runner/interface";
import { MailerCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DELIVERY from "../../assets/icon-delivery.svg";
import ICON_NO_DELIVERY from "../../assets/icon-no-delivery.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:mailer", "Send email");
    },
    kind: "headless",
})
export class Mailer extends NodeBlock implements IMailer {
    @definition
    recipient: string | "fixed" = "";

    @definition
    recipientFixed?: string;

    @definition
    @affects("#slots")
    sender?: string | "fixed";

    @definition
    senderFixed?: string;

    @definition
    @affects("#slots")
    alias?: string;

    @definition
    @affects("#slots")
    includeData?: boolean;

    @definition
    @affects("#slots")
    recipientExportable?: boolean;

    @definition
    @affects("#slots")
    senderExportable?: boolean;

    @definition
    @affects("#slots")
    subjectExportable?: boolean;

    @definition
    @affects("#slots")
    messageExportable?: boolean;

    recipientSlot!: Slots.String;

    @slots
    defineSlot(): void {
        this.recipientSlot = this.slots.static({
            type: Slots.String,
            reference: "recipient",
            label: pgettext("block:mailer", "Recipient"),
            alias: this.alias,
            exportable: this.recipientExportable,
            actionable: true,
        });

        this.slots.static({
            type: Slots.String,
            reference: "subject",
            label: pgettext("block:mailer", "Subject"),
            alias: this.alias,
            exportable: this.subjectExportable,
            actionable: true,
        });

        this.slots.static({
            type: Slots.Text,
            reference: "message",
            label: pgettext("block:mailer", "Message"),
            alias: this.alias,
            exportable: this.messageExportable,
            actionable: true,
        });

        if (this.sender) {
            this.slots.static({
                type: Slots.String,
                reference: "sender",
                label: pgettext("block:mailer", "Sender"),
                alias: this.alias,
                exportable: this.senderExportable,
                actionable: true,
            });
        } else {
            this.slots.delete("sender");
        }

        if (this.includeData) {
            this.slots.static({
                type: Slots.Boolean,
                reference: "data",
                label: "Data",
                pipeable: false,
                exportable: false,
                actionable: true,
            });
        } else {
            this.slots.delete("data");
        }
    }

    @editor
    defineEditor(): void {
        const recipients = filter(
            populateSlots(this, {
                blocks: ["tripetto-block-email", "tripetto-block-hidden-field"],
                mode: "validated",
            }),
            (slot) => (slot.id ? true : false)
        );

        const recipientFixed = new Forms.Email(
            Forms.Email.bind(this, "recipientFixed", undefined, "")
        )
            .placeholder(
                pgettext("block:mailer", "Type recipient email address here...")
            )
            .autoValidate()
            .require()
            .visible(this.recipient === "fixed");

        const senderFixed = new Forms.Email(
            Forms.Email.bind(this, "senderFixed", undefined, "")
        )
            .placeholder(
                pgettext("block:mailer", "Type sender email address here...")
            )
            .autoValidate()
            .require()
            .visible(this.sender === "fixed");

        this.editor.option({
            name: pgettext("block:mailer", "Recipient"),
            form: {
                title: pgettext("block:mailer", "Recipient"),
                controls: [
                    new Forms.Static(
                        pgettext(
                            "block:mailer",
                            "This block sends an email to the specified recipient. You can use any email block to define the recipient, as long as the block is inserted before this block. Supply a fixed recipient when the block should always mail to the same email address ([learn more](%1)).",
                            "https://tripetto.com/help/articles/how-to-use-the-send-email-block/"
                        )
                    ).markdown(),
                    new Forms.Dropdown(
                        [
                            ...(recipients.length > 0
                                ? [
                                      {
                                          optGroup: pgettext(
                                              "block:mailer",
                                              "Available email addresses"
                                          ),
                                      },
                                      ...map(recipients, (slot) => ({
                                          label: slot.label,
                                          value: slot.id || "",
                                      })),
                                      {
                                          optGroup: pgettext(
                                              "block:mailer",
                                              "Other"
                                          ),
                                      },
                                  ]
                                : []),
                            {
                                label: pgettext(
                                    "block:mailer",
                                    "Fixed recipient address"
                                ),
                                value: "fixed",
                            },
                        ],
                        Forms.Dropdown.bind(this, "recipient", "")
                    ).on((recipient: Forms.Dropdown<string>) => {
                        recipientFixed.visible(recipient.value === "fixed");
                    }),
                    recipientFixed,
                ],
            },
            activated: true,
            locked: true,
        });

        this.editor.name(
            false,
            false,
            pgettext("block:mailer", "Subject"),
            "validated"
        );
        this.editor.description(
            true,
            pgettext("block:mailer", "Message"),
            "validated"
        );

        this.editor.groups.settings();
        this.editor
            .option({
                name: pgettext("block:mailer", "Sender"),
                form: {
                    title: pgettext("block:mailer", "Sender"),
                    controls: [
                        new Forms.Static(
                            pgettext(
                                "block:mailer",
                                "You can specify a sender address. This address will be used to set the reply-to header of the email ([learn more](%1)).",
                                "https://tripetto.com/help/articles/how-to-use-the-send-email-block/#sender"
                            )
                        ).markdown(),
                        new Forms.Dropdown(
                            [
                                ...(recipients.length > 0
                                    ? [
                                          {
                                              optGroup: pgettext(
                                                  "block:mailer",
                                                  "Available email addresses"
                                              ),
                                          },
                                          ...map(recipients, (slot) => ({
                                              label: slot.label,
                                              value: slot.id || "",
                                          })),
                                          {
                                              optGroup: pgettext(
                                                  "block:mailer",
                                                  "Other"
                                              ),
                                          },
                                      ]
                                    : []),
                                {
                                    label: pgettext(
                                        "block:mailer",
                                        "Fixed sender address"
                                    ),
                                    value: "fixed",
                                },
                            ],
                            Forms.Dropdown.bind(this, "sender", undefined, "")
                        ).on((sender: Forms.Dropdown<string | undefined>) => {
                            senderFixed.visible(sender.value === "fixed");
                        }),
                        senderFixed,
                    ],
                },
                activated: isString(this.sender),
            })
            .onToggle((sender) => {
                if (exportables.card instanceof Forms.Form) {
                    exportables.card.control(3)?.visible(sender.isActivated);
                }
            });
        this.editor.option({
            name: pgettext("block:mailer", "Form data"),
            form: {
                title: pgettext("block:mailer", "Append form data"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:mailer",
                            "Append all form data to the message"
                        ),
                        Forms.Checkbox.bind(
                            this,
                            "includeData",
                            undefined,
                            true
                        )
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:mailer",
                            "This will append _all_ the exportable data of the form to the message. Useful, for example, if you want to send a copy of the form data to the respondent. If you just want to include certain form data, use the `@` sign in the message field."
                        )
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.includeData),
        });

        this.editor.groups.options();

        this.editor.visibility();
        this.editor.alias(this);

        const dataWarning = this.editor
            .form({
                controls: [
                    new Forms.Notification(
                        pgettext(
                            "block:mailer",
                            "By default the data generated by this block (recipient address, subject, message and optionally sender address) will not be stored in the dataset. To change this behavior, please use the *exportability* feature."
                        ),
                        "info"
                    ).markdown(),
                ],
            })
            .visible(!isBoolean(this.recipientExportable));

        const exportables = this.editor
            .exportable(
                {
                    ref: this,
                    props: [
                        {
                            name: "recipientExportable",
                            label: pgettext(
                                "block:mailer",
                                "Include recipient address in the dataset"
                            ),
                        },
                        {
                            name: "subjectExportable",
                            label: pgettext(
                                "block:mailer",
                                "Include subject in the dataset"
                            ),
                        },
                        {
                            name: "messageExportable",
                            label: pgettext(
                                "block:mailer",
                                "Include message in the dataset"
                            ),
                        },
                        {
                            name: "senderExportable",
                            label: pgettext(
                                "block:mailer",
                                "Include sender address in the dataset"
                            ),
                        },
                    ],
                },
                false
            )
            .onToggle((exportable) =>
                dataWarning.visible(!exportable.isActivated)
            );

        if (exportables.card instanceof Forms.Form) {
            exportables.card.control(3)?.visible(isString(this.sender));
        }
    }

    @conditions
    defineConditions(): void {
        each(
            [
                {
                    label: pgettext("block:mailer", "Mail will be send"),
                    icon: ICON_DELIVERY,
                    willMail: true,
                },
                {
                    label: pgettext("block:mailer", "No mail will be send"),
                    icon: ICON_NO_DELIVERY,
                    willMail: false,
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: MailerCondition,
                    label: condition.label,
                    icon: condition.icon,
                    props: {
                        slot: this.recipientSlot,
                        willMail: condition.willMail,
                    },
                });
            }
        );
    }
}
