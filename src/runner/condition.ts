/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { IMailerCondition } from "./interface";
import { IS_EMAIL } from "./regex";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class MailerCondition extends ConditionBlock<IMailerCondition> {
    @condition
    willMail(): boolean {
        const recipientSlot = this.valueOf<string>("recipient");

        if (this.props.willMail) {
            return (
                (recipientSlot &&
                    recipientSlot.hasValue &&
                    IS_EMAIL.test(recipientSlot.string)) ||
                false
            );
        } else {
            return !recipientSlot || recipientSlot.string === "";
        }
    }
}
