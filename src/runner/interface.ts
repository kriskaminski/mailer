export interface IMailer {
    recipient: string | "fixed";
    recipientFixed?: string;
    sender?: string | "fixed";
    senderFixed?: string;
    includeData?: boolean;
}
export interface IMailerCondition {
    willMail: boolean;
}
