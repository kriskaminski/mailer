/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    HeadlessBlock,
    Slots,
    assert,
    processor,
    tripetto,
    validator,
} from "tripetto-runner-foundation";
import { IMailer } from "./interface";
import { IS_EMAIL } from "./regex";
import "./condition";

@tripetto({
    type: "headless",
    identifier: PACKAGE_NAME,
})
export class MailerBlock extends HeadlessBlock<IMailer> {
    readonly recipientSlot = assert(
        this.valueOf<string, Slots.String>("recipient")
    );
    readonly subjectSlot = assert(
        this.valueOf<string, Slots.String>("subject")
    );
    readonly messageSlot = assert(
        this.valueOf<string, Slots.String>("message")
    );
    readonly senderSlot = this.valueOf<string, Slots.String>("sender");
    readonly dataSlot = this.valueOf<boolean, Slots.Boolean>("data");

    @processor
    do(): void {
        let recipient = "";

        if (this.props.recipient && this.props.recipient !== "fixed") {
            const value = this.variableFor(this.props.recipient);

            if (value) {
                recipient = value.string;
            }
        } else {
            recipient = this.props.recipientFixed || "";
        }

        this.recipientSlot.value = recipient || "";
        this.subjectSlot.value = this.parseVariables(this.node.name || "", "-");
        this.messageSlot.value = this.parseVariables(
            this.node.description || "",
            "-",
            true
        );

        if (this.senderSlot) {
            let sender = "";

            if (this.props.sender && this.props.sender !== "fixed") {
                const value = this.variableFor(this.props.sender);

                if (value) {
                    sender = value.string;
                }
            } else {
                sender = this.props.senderFixed || "";
            }

            this.senderSlot.value = (IS_EMAIL.test(sender) && sender) || "";
        }

        if (this.dataSlot && this.props.includeData) {
            this.dataSlot.value = true;
        }
    }

    @validator
    validate(): boolean {
        return (
            !this.recipientSlot.string ||
            IS_EMAIL.test(this.recipientSlot.string)
        );
    }
}
